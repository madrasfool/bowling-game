package com.interview.bowling;

/**
 * Bowling App
 *
 */
public class BowlingApp
{
    public static void main( String[] args )
    {
        java.util.List<java.util.List<Integer> > samples = new java.util.ArrayList<>(
                java.util.Arrays.asList(
                        //java.util.Arrays.asList(10, 0, 0, 10),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4),
                        java.util.Arrays.asList(9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(9, 1, 10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 1, 9),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 8, 1),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 9, 1),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 1, 10),
                        java.util.Arrays.asList(10,10,10,10,10,10,10,10,10,10,10,10)
                )
        );

        for(int idx=0; idx<samples.size(); idx++) {
            BowlingGame game = new BowlingGame();
            for (Integer i : samples.get(idx)) {
                game.roll(i.intValue());
            }

            System.out.println("Is finished: "+game.isFinished());
            System.out.println("score: "+game.getScore());
        }
     }
}
