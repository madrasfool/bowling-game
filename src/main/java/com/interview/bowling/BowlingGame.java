package com.interview.bowling;


/**
 * Created by yeshwanthsriram on 2/11/16.
 */
public class BowlingGame {

    /**
     * Enum for marking a frame as strike, spare, or neither
     */
    private enum StrikeOrSpare{
        STRIKE, SPARE, NONE;
    };

    /**
     * Enum for valid pins. Maybe overkill.
     */
    private enum ValidScore {
        ZERO(0),
        ONE(1),
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10);

        private final int pins;

        ValidScore(int pins){ this.pins = pins;}

        public int n(){return this.pins;}
    };

    /**
     * Class to represent a frame that is either completed or in progress.
     */
    private class Frame {
        private final int MAX_ROLLS = 2;
        private java.util.ArrayList <Integer> rolls = new java.util.ArrayList<>(0);
        private StrikeOrSpare strikeOrSpare;
        private int rollCount = 0;
        private boolean isBonusFrame = false;

        /**
         * Constructor with flag parameter to indicate if this is a bonus frame.
         * @param bonusFrame
         */
        Frame(boolean bonusFrame){
            this.isBonusFrame = bonusFrame;
            this.strikeOrSpare = StrikeOrSpare.NONE;
        }


        /**
         * Does nothing if there are no more rolls left otherwise will push the score
         * onto a `rolls` stack.
         * @param points
         */
        public void roll(int points){
            if(hasRolls()) {
                final int size = rolls.size();
                rolls.add(points);
                if (size == 0 && points == ValidScore.TEN.n()) /*if we have a strike*/ {
                    if(!isBonusFrame){rolls.add(0);}
                    strikeOrSpare = StrikeOrSpare.STRIKE;
                } else if (size == 1 && (points + getScore(1)) == ValidScore.TEN.n()) /*if we have a spare*/ {
                    strikeOrSpare = StrikeOrSpare.SPARE;
                }
                rollCount += 1;
            }
        }

        private boolean hasRolls(){return rolls.size() < MAX_ROLLS;}

        private int getRollCount(){return rollCount;}

        public StrikeOrSpare getStrikeOrSpare(){return strikeOrSpare;}

        public int getScore(){return(getScore(rolls.size()));}

        public int getScore(int upto){
            return(rolls.subList(0,upto).stream().mapToInt(Integer::intValue).sum());
        }

        public boolean getIsBonusFrame(){return isBonusFrame;}

        public void dump(){
            System.out.println("  Frame:");
            System.out.println("   Score: "+getScore());
            System.out.println("   StrikeOr? "+getStrikeOrSpare());
            System.out.println("   RollCount: "+getRollCount());
        }
    }


    /**
     * Private members and constants for the main BowlingGame class.
     */
    private final static int MAX_FRAMES = 10;
    private java.util.Optional<Frame> currentFrame;
    private java.util.ArrayList<Frame> frames = new java.util.ArrayList<>(0);

    public BowlingGame(){currentFrame = java.util.Optional.empty();}


    /**
     * Method to simulate a roll in frame. TBD: Note that the method does not validate
     * if the game is finished are not.
     * @param points
     */
    public void roll(final int points){
        //if(isFinished()){return;}
        if(!isValidScore(points)){return;}

        if(!currentFrame.isPresent()){allocateNewFrame(points);}

        Frame frame = currentFrame.get();
        frame.roll(points);

        if(!frame.hasRolls() && frames.size() < MAX_FRAMES){
            frames.add(frame);
            clearCurrentFrame();
        }
    }

    /**
     * Method to compute the score for a game. TBD: Index bounds checks for
     * under and over spills.
     * @return finalScore for game
     */
    public int getScore(){
        int result = 0;
        for(int idx=0; idx < frames.size(); idx++){
            final Frame frame = frames.get(idx);

            int sc = frame.getScore();
            result+= sc;

            StrikeOrSpare strikeOrSpare = frame.getStrikeOrSpare();

            if(strikeOrSpare == StrikeOrSpare.STRIKE){
                if(idx == MAX_FRAMES-1) /*if last frame*/ {
                    sc = currentFrame.get().getScore();
                }else if(idx == MAX_FRAMES-2) /*if second to last frame*/{
                    final Frame framePlus = frames.get(MAX_FRAMES-1);
                    final Frame framePlusPlus = currentFrame.get();

                    sc = framePlus.getStrikeOrSpare() == StrikeOrSpare.STRIKE ?
                            (ValidScore.TEN.n() + framePlusPlus.getScore(1)) : framePlus.getScore();
                }else {
                    final Frame strikePlus = idx+1 < frames.size() ? frames.get(idx+1) : null;
                    final Frame strikePlusPlus = strikePlus!=null ? frames.get(idx+2) : null;


                    if(strikePlus !=null && strikePlus.getStrikeOrSpare() == StrikeOrSpare.STRIKE){
                        sc = ValidScore.TEN.n() + strikePlusPlus.getScore();
                    }else if (strikePlus != null){
                        sc = strikePlus.getScore();
                    }
                }
                result += sc;
            }else if(strikeOrSpare == StrikeOrSpare.SPARE){
                if(idx == MAX_FRAMES-1) {
                    sc = currentFrame.get().getScore(1);
                }else {
                    sc = idx+1 < frames.size() ? frames.get(idx+1).getScore(1) : 0;
                }
                result += sc;
            }
        }
        return result;
    }

    /**
     * Return if the game is finished including bonus frame.
     * @return true/false
     */
    public boolean isFinished(){

        if(frames.size() == MAX_FRAMES) {
            //bonus round check
            Frame frame = currentFrame.isPresent() ? currentFrame.get() : null;
            Frame finalFrame = getFinalFrame();
            if(frame != null && frame.getIsBonusFrame()) {
                //special case!
                return((finalFrame.getStrikeOrSpare() == StrikeOrSpare.STRIKE && frame.getRollCount() == 2)
                        || (finalFrame.getStrikeOrSpare() == StrikeOrSpare.SPARE && frame.getRollCount() == 1));
            }else if(finalFrame!=null){
                return isFrameOver(finalFrame);
            }
        }else {
            Frame frame = currentFrame.isPresent() ? currentFrame.get() : null;
            if(frame!=null){
                return isFrameOver(frame);
            }
        }
        return false;
    }


    public void dump(){
        System.out.println("Number Of Frames: "+frames.size());
        for(Frame frame: frames) {
            frame.dump();
        }
        if(currentFrame.isPresent()){
            System.out.println("Bonus frame:");
            currentFrame.get().dump();
        }
    }

    //========== private methods ===========================================

    private void clearCurrentFrame(){currentFrame = java.util.Optional.empty();}

    private void allocateNewFrame(int points){

        Frame frame = getFinalFrame();
        boolean isBonus = getFinalFrame()!=null;
        currentFrame = java.util.Optional.of(new Frame(isBonus));
    }

    private boolean isFrameOver(Frame frame){
        return((frame.getStrikeOrSpare() == StrikeOrSpare.STRIKE && frame.getRollCount() == 2)
                || (frame.getStrikeOrSpare() == StrikeOrSpare.SPARE && frame.getRollCount() == 1)
                || (frame.getRollCount() == 2)
        );
    }

    private boolean isValidScore(int score){
        if(currentFrame.isPresent() && !currentFrame.get().getIsBonusFrame()) {
            score += currentFrame.get().getScore(1);
        }
        return (score >= ValidScore.ZERO.n() && score <= ValidScore.TEN.n());
    }

    private boolean isFinalFrame(){return(frames.size() == MAX_FRAMES);}

    private Frame getFinalFrame(){
        return(isFinalFrame() ? frames.get(MAX_FRAMES-1) : null);
    }
}
