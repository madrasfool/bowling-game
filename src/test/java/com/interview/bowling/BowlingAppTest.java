package com.interview.bowling;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple BowlingApp.
 */
public class BowlingAppTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BowlingAppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( BowlingAppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testBowlingApp()
    {
        java.util.List<Integer> expected = new java.util.ArrayList<>(
                java.util.Arrays.asList(0,80,28,28,48,48,57,60,19,19,20,20,300)
        );

        java.util.List<java.util.List<Integer> > samples = new java.util.ArrayList<>(
                java.util.Arrays.asList(
                        //java.util.Arrays.asList(10),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4),
                        java.util.Arrays.asList(9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(9, 1, 10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 9, 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(10, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 1, 9),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 8, 1),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 9, 1),
                        java.util.Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 1, 10),
                        java.util.Arrays.asList(10,10,10,10,10,10,10,10,10,10,10,10)
                )
        );

        for(int idx=0; idx<samples.size(); idx++) {
            BowlingGame game = new BowlingGame();
            for (Integer i : samples.get(idx)) {
                game.roll(i.intValue());
            }
            assertEquals(expected.get(idx).intValue(),game.getScore());
            assertEquals(true,game.isFinished());
        }
    }
}
