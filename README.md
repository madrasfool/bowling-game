## BowlingGame
Write a Java program that calculates the score of a game of bowling (also known as “ten-pin bowling”).

### Assumptions/Bugs

- No validation if rolls exceed the max 10 or the plus+1 bonus
- Does not work for under-spill scores (however it won't crash)

### Build/Test

```
$ mvn compile|package|test
```

### Test Data

See @BowlingAppTest.java